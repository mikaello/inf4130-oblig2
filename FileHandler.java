import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class FileHandler {
    /**
     * Reads a file in the following format:
     *
     *  N
     *  x x x
     *  x x x
     *  x x x
     *
     * Where N is a integer determining how big the following NxN
     * matrix of integers is.
     *
     * @param filename file to be read
     * @return a double-byte-array filled with the NxN matrix
     */
    public static byte[][] readFile(String filename) {
	Scanner scan = null;
	int n = 0; /* Number of rows / columns */

        try {
            scan = new Scanner(new File(filename));
        } catch (FileNotFoundException e) {
	    e.printStackTrace();
	    System.exit(1);
	}

	if (scan.hasNextInt()) {
	    n = scan.nextInt();
	} else {
	    System.err.println("Empty file: " + filename);
	}

	byte[][] puzzle = new byte[n][n];
	for (int i = 0; scan.hasNextByte(); i++) {
	    puzzle[i/n][i%n] = scan.nextByte();
	}

	return puzzle;
    }

    /**
     * Writing to outputfile
     */
    public static void writeToFile(String filename, int stepsToSolution, 
				   String solution, int visitedStates,
				   int totalNumOfStates, int numOfModificationss) {
	PrintWriter out = null;
	
	try {
	    out = new PrintWriter(new File(filename));
	    out.println(stepsToSolution);
	    out.println(solution);
	    out.println(visitedStates);
	    out.println(totalNumOfStates);
	    out.println(numOfModificationss);

	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    System.exit(1);
	} finally {
	    out.close();
	}

	

    }
}
