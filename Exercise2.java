import java.util.PriorityQueue;
import java.util.HashSet;
import java.util.HashMap;

/**
 * Created 2013/10/14 15:31:06
 * @author mikaello@foresight.ifi.uio.no
 */
public class Exercise2 {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("Usage: java Oblig2 <input> <output>");
            System.exit(1);
        }

        byte[][] puzzle = FileHandler.readFile(args[0]);

	System.out.println("Solving file:          " + args[0] + "\n");

	new SolvePuzzle(puzzle, args[1]).run();
    }

}


/**
 * This class is administrating the solving
 */
class SolvePuzzle {
    private String outputFilename;

    /* The initial puzzle read from file */
    private State originalState;

    /* Closed states */
    private HashSet<State> closedStates = new HashSet<State>();

    /* Open states (using HashMap as a hack to retrieve the element) */
    private HashMap<State,State> openStates = new HashMap<State,State>();

    /* A priority queue used for finding the best solution */
    private PriorityQueue<State> queue = new PriorityQueue<State>();

    /* Start time of the algorithm */
    private long startTime;

    private int numberOfUpdatedCost = 0;
    private int numberOfAddToQueue = 0;
    private int numberOfVisitedStates = 1;

    SolvePuzzle(byte[][] originalPuzzle, String outputFilename) {
	originalState = new State(originalPuzzle);
        this.outputFilename = outputFilename;
        queue.add(new State(originalState));
    }

    public void run() {
	/* Print start state */
	System.out.println(originalState);

        State current = null;

        /* Start timer */
        startTime = System.currentTimeMillis();

        do {
            current = queue.poll();        /* Getting node with smallest cost */
            numberOfVisitedStates++;
            openStates.remove(current);    /* No longer open, but closed */
            closedStates.add(current);     /* Adding to closed list */
            extractNeighbours(current);    /* Adding neighbours to queue */
        } while (current != null);

	if (current == null) {
	    // No solution to this puzzle
	    System.out.println("THIS PUZZLE IS UNSOLVABLE ");
	    endFunction(originalState);
	}
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (State s : queue) {
            sb.append(s.toString());
        }

        return sb.toString();
    }

    private void extractNeighbours(State s) {
	if (s == null) {
	    // Unsolvable
	    return;
	}

        State up = s.getUp();
        State down = s.getDown();
        State right = s.getRight();
        State left = s.getLeft();

        examineState(up);
        examineState(down);
        examineState(right);
        examineState(left);
    }

    private void examineState(State s) {
        /* Outside grid or is an closed state */
        if (s == null || closedStates.contains(s)) {
            return;
        } else if (s.getHeuristicSum() == 0) {
            endFunction(s);
        }


        /* Already in priority queue */
        State temp = openStates.get(s);
        if (temp != null) {

            /* Check to see which has the best path */
            if (temp.compareToDistance(s) < 0) {
                return;               /* The existing has best path */
            }  else {
                numberOfUpdatedCost++;
                queue.remove(temp);   /* The new state has a better path */
                openStates.remove(temp);
            }
        }

        numberOfAddToQueue++;
        queue.add(s);
        openStates.put(s,s);
    }

    /**
     * Sums up the info and exits the program
     */
    private void endFunction(State endState) {
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;

        System.out.println("Duration of solving:   " + ((double)duration / 1000.0) + " seconds");
        System.out.println("Total steps used:      " + endState.getDistance());
        System.out.println("Total states found:    " + State.getTotalStates());
	System.out.println("Total updates of cost: " + numberOfUpdatedCost);
	System.out.println("Written to file:       " + outputFilename);

        FileHandler.writeToFile(outputFilename, endState.getDistance(),
                                endState.getSolution(), numberOfVisitedStates,
                                numberOfAddToQueue, numberOfUpdatedCost);

        /* Exit program, goal state is reached */
        System.exit(0);
    }
}
