/**
 * Denotes a position in a grid. Where the first position in a grid is
 * (0,0)
 */
public class Position {
    private int x, y;

    /** Copy-constructor */
    Position(Position p) {
        this(p.x, p.y);
    }

    Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Calculate the manhattan distance between to points. This is the
     * shortest way between two points in a grid, as described here:
     * http://en.wikipedia.org/wiki/Taxicab_geometry
     *
     * Example grid:
     *
     *    . x . . .
     *    . . . . .
     *    . . . . x
     *
     * The manhattan distance between these points is 5 (3 horizontal
     * + 2 vertical)
     *
     * @param p the point to be calculated distance to
     * @return the manhattan distance from this point to the point given
     */
    public int manhattanDistance(Position p) {
        int horizontalDistance = Math.abs(x - p.x);
        int verticalDistance = Math.abs(y - p.y);

        return verticalDistance + horizontalDistance;
    }

    public int getX() { return x; }
    public int getY() { return y; }

    public Position up() {
        int newX = x - 1;

        if (newX < 0) {
            return null;
        } else {
            return new Position(newX, y);
        }
    }

    public Position down(int border) {
        int newX = x + 1;

        if (newX >= border) {
            return null;
        } else {
            return new Position(newX, y);
        }
    }

    public Position left() {
        int newY = y - 1;

        if (newY < 0) {
            return null;
        } else {
            return new Position(x, newY);
        }
    }

    public Position right(int border) {
        int newY = y + 1;

        if (newY >= border) {
            return null;
        } else {
            return new Position(x, newY);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }

        Position pos = (Position) obj;

        return (x == pos.x) && (y == pos.y);
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }

    /** Used for hashCode in State */
    public int addValue() {
        return x+y;
    }
}
