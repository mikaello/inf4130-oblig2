import java.util.Formatter;

/**
 * This class contains a specific state
 */
public class State implements Comparable<State> {
    /* A current state of a puzzle */
    private byte[][] puzzle;
    private Position currentPosition;

    /* Distance to this state (number of moves) */
    private int distance;

    /* Temporary variable for a calculated heuristic, used for less calculations */
    private int tempHeuristicSum;

    /* Total states created */
    private static int totalNumberOfStates = 0;

    /* Parent to this state in the solution */
    private State parent;

    /* String representing the solution */
    private String solution = "";

    /* Variable by hashCode(), to not compute more times than necessary */
    private int hashCode = -1;

    /* Variable to check if there have been movements on this board,
       since it is kind of a movement when you initialize, it is
       initialized to true */
    private boolean moved = true;

    State(State s) {
        this(s.puzzle, s.currentPosition, s.distance);
        parent = s;
        solution = s.solution;
    }

    State(byte[][] puzzle) {
        this(puzzle, State.findCurrentPosition(puzzle));
    }

    State(byte[][] puzzle, Position currentPosition) {
        this(puzzle, currentPosition, 0);
    }

    State(byte[][] puzzle, Position currentPosition, int distance) {
        this.puzzle = makeCopy(puzzle);
        this.currentPosition = new Position(currentPosition);
        this.distance = distance;

        if (distance == 0) {
            /* If distance is zero, it needs a heuristic sum */
            tempHeuristicSum = heuristicSum();
        }

        totalNumberOfStates++;
    }

    /**
     * Makes a deep copy of a double byte-array
     * @param to be copied
     * @return deep copy of the parameter
     */
    private byte[][] makeCopy(byte[][] puzzle) {
        byte[][] copy = new byte[puzzle.length][puzzle[0].length];

        for (int i = 0; i < puzzle.length; i++) {
            for (int j = 0; j < puzzle[i].length; j++) {
                copy[i][j] = puzzle[i][j];
            }
        }

	return copy;
    }

    /**
     * @return position when 0 is found, null if not found
     */
    public static Position findCurrentPosition(byte[][] puzzle) {
        for (int i = 0; i < puzzle.length; i++) {
            for (int j = 0; j < puzzle[i].length; j++) {
                if (puzzle[i][j] == 0) {
                    return new Position(i,j);
                }
            }
        }

        return null;
    }

    /**
     * Finds the heuristic sum of this puzzle by finding the heuristic
     * sum for each element and sums these up.
     */
    int heuristicSum() {
        int sum = 0;

        for (int i = 0; i < puzzle.length; i++) {
            for (int j = 0; j < puzzle[i].length; j++) {
                if (puzzle[i][j] == 0) {
                    /* Current position, go on */
                    continue;
                }

                Position correctPosition = findRightPositionInPuzzle(puzzle[i][j]);
                Position puzzlePosition = new Position(i,j);

                if (!puzzlePosition.equals(correctPosition)) {
                    sum += correctPosition.manhattanDistance(puzzlePosition);
                }
            }
        }

        return sum;
    }

    /**
     * Finds the correct position for a value based on this order of
     * the numbers:
     *
     *   1  2  3  4
     *   5  6  7  8
     *   9  10 11 12
     *   13 14 15 0
     *
     * For example, if 7 is given as value, the position with indices
     * (1,2) is returned (because 7 has the correct position (1,2) in
     * our board).
     *
     * @param value to be placed
     * @return the correct position of value
     */
    Position findRightPositionInPuzzle(int value) {
        // - 1 because indices beginn at 0
        int row = (value-1) / puzzle.length;
        int column = (value-1) % puzzle[0].length;

        return new Position(row, column);
    }

    public void updateSolution(String updateDirection) {
        solution = solution + updateDirection;
    }

    /**
     * @return negative number if this element is before the argument,
     * 0 if equal and positive if this element has a higher heuristic
     * value than the argument.
     */
    @Override
        public int compareTo(State s) {
        if (s == null) {
            throw new NullPointerException();
        } else if (!(s instanceof State)) {
            throw new ClassCastException();
        }

        return (tempHeuristicSum - s.tempHeuristicSum) + (distance - s.distance);
    }

    /**
     * Works like compareTo(), but does only consider distance.
     */
    public int compareToDistance(State s) {
        if (s == null) {
            throw new NullPointerException();
        } else if (!(s instanceof State)) {
            throw new ClassCastException();
        }

        return distance - s.distance;
    }

    /**
     * This method is used for debug and printing the full path of
     * states to the goal
     * @param num number of steps from the goal to this state
     */
    public void backtrack(int num) {
        System.out.println(this);
        if (parent != null) {
            parent.backtrack(num+1);
        } else {
            System.out.println("Number of steps: " + num);
        }
    }

    /**
     * @return the state to the left
     */
    public State getLeft() {
        return getStateOfDirection(currentPosition.left(), "L");
    }

    /**
     * @return the state to the right
     */
    public State getRight() {
        return getStateOfDirection(currentPosition.right(puzzle.length), "R");
    }

    /**
     * @return the state upwards
     */
    public State getUp() {
        return getStateOfDirection(currentPosition.up(), "U");
    }

    /**
     * @return the state downwards
     */
    public State getDown() {
        return getStateOfDirection(currentPosition.down(puzzle.length), "D");
    }

    /**
     * @return the state of this position, switched from current position
     */
    public State getStateOfDirection(Position p, String direction) {
        if (p == null) {
            // Position is out of grid
            return null;
        }

        State newState = new State(this);
        newState.movePosition(p);
        newState.updateSolution(direction);

        return newState;
    }

    /**
     * Move position from from current position to "b", this
     * increments the distance with 1, changes the current position
     * and calculates new heuristic sum
     */
    private void movePosition(Position newPos) {
        byte temp = puzzle[currentPosition.getX()][currentPosition.getY()];
        puzzle[currentPosition.getX()][currentPosition.getY()] = puzzle[newPos.getX()][newPos.getY()];
        puzzle[newPos.getX()][newPos.getY()] = temp;

        currentPosition = new Position(newPos);
        tempHeuristicSum = heuristicSum();
        moved = true; // Used by hashCode, new hashCode has to be calculated when moved
        distance++;
    }

    /**
     * Returns a textual representation of this puzzle, with "x" as a
     * symbol for current position.
     */
    @Override
        public String toString() {
        StringBuilder sb = new StringBuilder();
        Formatter f = new Formatter(sb);

        sb.append("\n");
        for (int i = 0; i < puzzle.length; i++) {
            sb.append("  ");
            for (int j = 0; j < puzzle[i].length; j++) {
                if (currentPosition.equals(new Position(i,j))) {
                    f.format("%2s ", "");
                } else {
                    f.format("%2d ", puzzle[i][j]);
                }
            }
            f.format("\n");
        }

        f.format("\nCurrent position is %s, heuristic sum: %d, distance: %d\n",
                 currentPosition, tempHeuristicSum, distance);

        return sb.toString();
    }

    /**
     * Returns a hashcode, calculates it if there has been movements
     * in the board. This is a very expensive calculator, but I
     * haven't found a bettery solution.
     */
    public int hashCode() {
        // Moved is set to true when a move is made (and in initialization)
        if (moved) {
            int count = 0;
            long temp = 0;
            for (int i = 0; i < puzzle.length; i++) {
                for (int j = 0; j < puzzle[i].length; j++) {
                    // Generating the hashvalue:
                    if (temp >= (Integer.MAX_VALUE)) {
                        temp = (temp % Integer.MAX_VALUE) + currentPosition.addValue();
                    }
                    temp += puzzle[i][j]*(10*count++);
                }
            }

            hashCode = (int) (temp % Integer.MAX_VALUE);

            moved = false; // Used to track movements
        }

        return hashCode;
    }

    /**
     * @return true if the puzzle of this element is equal to the one
     * given as parameter
     */
    @Override
        public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }

        State s = (State) obj;

        for (int i = 0; i < puzzle.length; i++) {
            for (int j = 0; j < puzzle[i].length; j++) {
                if (puzzle[i][j] != s.puzzle[i][j]) {
                    return false;
                }
            }
        }

        return true;
    }

    /** @return total number of states */
    public static int getTotalStates() { return totalNumberOfStates; }

    /** @return distance from origin to this (steps) */
    public int getDistance() { return distance; }

    /** @return the heuristic sum of this board */
    public int getHeuristicSum() { return tempHeuristicSum; }

    /** @return the textual representation of the steps to solution */
    public String getSolution() { return solution; }
}
